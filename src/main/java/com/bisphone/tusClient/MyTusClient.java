package com.bisphone.tusClient;

import io.tus.java.client.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class MyTusClient {

    private TusClient client;
    private TusUpload upload;
    private TusURLMemoryStore urlMemoryStore;
    private URL url;

    public MyTusClient() throws MalformedURLException, FileNotFoundException {
        String fileName = "film.mkv";
        url = new URL("http://localhost:8080/files");
        File file = new File("src/test/resources/" + fileName);
        client = new TusClient();
        client.setUploadCreationURL(url);
        urlMemoryStore = new TusURLMemoryStore();
        upload = new TusUpload(file);
        client.enableResuming(urlMemoryStore);

    }


    public void simpleUpload() throws IOException, ProtocolException {
        TusExecutor executor = new MyTusExecuter(client, upload);
        System.out.println("uploading ...");
        executor.makeAttempts();
    }

    public void uploadResume() throws IOException, ProtocolException {

        urlMemoryStore.set(upload.getFingerprint()
                , new URL(url + "/" + upload.getEncodedMetadata().split(" ")[1]));
        TusExecutor executor = new MyTusExecuter(client, upload);
        System.out.println("uploading ...");
        executor.makeAttempts();
    }
}
