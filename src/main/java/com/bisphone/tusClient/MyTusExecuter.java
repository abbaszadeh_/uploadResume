package com.bisphone.tusClient;

import io.tus.java.client.*;

import java.io.IOException;

public class MyTusExecuter extends TusExecutor {

    private TusClient client;
    private TusUpload upload;


    public MyTusExecuter(TusClient client, TusUpload upload) {
        this.client =client;
        this.upload = upload;
    }

    @Override
    protected void makeAttempt() throws ProtocolException, IOException {
        TusUploader uploader = client.resumeOrCreateUpload(upload);
        uploader.setChunkSize(1024 * 256);

        //handling terminations...
        //when any termination happened appended chunks uploaded so far..
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("termination happened!!!");
            try {
                uploader.finish();
            } catch (ProtocolException e) {
                System.err.println("termination");
                //e.printStackTrace();
            } catch (IOException e) {
                System.err.println("termination");
                //e.printStackTrace();
            }
        }));

        do {

            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            long totalBytes = upload.getSize();
            System.out.println("totalSize: " + totalBytes);
            long bytesUploaded = uploader.getOffset();
            System.out.println("bytesUploaded" + bytesUploaded);
            double progress = (double) bytesUploaded / totalBytes * 100;
            System.out.printf("%.2f%%.\n", progress);
        } while (uploader.uploadChunk() > -1);

        uploader.finish();
        // Allow the HTTP connection to be closed and cleaned up
        System.out.println("Upload finished.");
        System.out.format("Upload available at: %s", uploader.getUploadURL().toString());
    }
}
