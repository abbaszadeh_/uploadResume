package com.bisphone.manualUploading;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;

@RestController
@RequestMapping("/my/files")
public class Controller {

    @Autowired
    private MyService service;

    @PostMapping("upload")
    public Mono<ResponseEntity> uploadFile(MultipartFile file) {
        return service.fileUpload(file);
    }

    @PutMapping("/upload/resume")
    public Mono<ResponseEntity> uploadWithResume(@RequestPart("chunk") byte[] chunk,
                                                 @RequestPart("fileName") String fileName,
                                                 @RequestParam("length") Long length
//            ,@RequestHeader("content-range") String range
    ) throws ParseException {
        try {
            return service.fileResumeUpload(chunk, fileName, length);
        } catch (IOException e) {
            e.printStackTrace();
            return Mono.just(ResponseEntity.status(HttpStatus.PERMANENT_REDIRECT).build());
        }
    }

    @RequestMapping(value = "/get/uploaded/size", method = RequestMethod.HEAD)
    public Mono<ResponseEntity> getUploadedSize(@RequestParam("fileName") String fileName) throws IOException {
        if (Files.exists(Paths.get("src/main/resources/" + fileName))) {

            String size = String.valueOf(Files.size(Paths.get("src/main/resources/" + fileName)));

            return Mono.just(ResponseEntity.ok()
                    .header("upload-offset", size)
                    .build());
        } else{
            return Mono.just(ResponseEntity.notFound()
                    .header("upload-offset" , "0").build());
        }
    }
}
