package com.bisphone.manualUploading;

import java.io.InputStream;
import java.util.Date;

public class Attachment{
    private String originalName;
    private long totalSize;
    private long uploadedSize;
    private long lastModified;
    private InputStream inputStream;
    private Date expirationDate;
}
