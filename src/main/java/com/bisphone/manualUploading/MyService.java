package com.bisphone.manualUploading;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Service
public class MyService {


    public Mono<ResponseEntity> fileUpload(MultipartFile file){
        Path root = Paths.get("/home/hamed/uploadFile/upload1");
        try {
            System.out.println(file.getOriginalFilename());
            Path path = Files.createTempFile("_", file.getOriginalFilename());
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            root.resolve(file.getOriginalFilename());
            root = root.resolve(Paths.get(file.getOriginalFilename()));
            System.out.println(root);
            Files.createDirectories(root.getParent());
            Files.copy(path, root, StandardCopyOption.REPLACE_EXISTING);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return Mono.just(ResponseEntity.ok().build());
    }

    public Mono<ResponseEntity> fileResumeUpload(byte[] chunk , String fileName,long length) throws IOException, ParseException {
        BufferedOutputStream out
                = new BufferedOutputStream(new FileOutputStream("src/main/resources/" + fileName, true));

        boolean uploaded = true;

        try {
            out.write(chunk);
        } catch (IOException e) {
            uploaded = false;
            System.err.println("io exception");
        } finally {
            if (uploaded) {
                out.close();
                    return Mono.just(ResponseEntity.ok()
                            .header("expiration-date", getExpirationDate())
                            .build());
            } else {
                out.close();
                return Mono.just(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());
            }
        }
    }
    public static String getExpirationDate() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime());
        calendar.add(Calendar.MONTH, 1);
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        return format.format(calendar.getTime());
    }
}
