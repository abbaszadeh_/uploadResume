package com.bisphone.service;

import com.bisphone.storage.AttachmentStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;

@Service
public class TusService {

    @Autowired
    private AttachmentStorage storage ;

    public Mono<ResponseEntity> createUpload(String uploadMeta){
        System.out.println("upload Creating");
        String id = uploadMeta.split(" ")[1];
        return Mono.just(ResponseEntity.created(URI.create("http://192.168.104.129:8080/files/" + id))
                .header("Tus-Resumable","1.0.0")
                .header("Upload-Length",uploadMeta)
                .build());
    }

    public Mono<ResponseEntity> upload(String id,HttpServletRequest request , Long uploadLength) throws IOException {


        storage.write(request.getInputStream() , id);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Upload-Expires", getExpirationDate());


        long offset = storage.getUploadedSize(id);
        if (offset>-1) headers.set("Upload-Offset", String.valueOf(offset));


        System.out.println("\n");
        return Mono.just(ResponseEntity.noContent().headers(headers).build());
    }

    public Mono<ResponseEntity> headProcessing(Long length,String id,HttpServletRequest request) throws IOException {


        if (storage.fileExist(id)) {
            System.out.println("head processing");
            System.out.println();
            return Mono.just(ResponseEntity.status(HttpStatus.OK)
                    .header("Upload-Offset", String.valueOf(storage.getUploadedSize(id)))
                    .header("Upload-Length",String.valueOf(length))
                    .header("Cache-Control","no-store")
                    .build());
        } else {
            return Mono.just(ResponseEntity.notFound()
                    .header("Cache-Control","no-store")
                    .build());
        }

    }

    private String getExpirationDate(){
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.WEEK_OF_MONTH, 1);
        return dateFormat.format(calendar.getTime());
    }

    private void printAllHeaders(HttpServletRequest request){
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String header = headerNames.nextElement();
            System.out.println(header+": "+request.getHeader(header));
        }
    }

}
