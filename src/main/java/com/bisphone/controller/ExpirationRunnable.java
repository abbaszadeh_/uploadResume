package com.bisphone.controller;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

public class ExpirationRunnable implements Runnable {

    private String baseLocation = "/home/hamed/Downloads/myFiles";

    @Override
    public void run(){
        try {
            checkExpirationAndDelete();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void checkExpirationAndDelete() throws InterruptedException {
        File location = new File(baseLocation);
        File[] files = location.listFiles();
        Date now = new Date();

        //one week expiration!!!

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(now.getTime());
        calendar.add(Calendar.WEEK_OF_MONTH, -1);

        Date fileLastModified;

        while (true) {
            for (File file : files) {
                fileLastModified = new Date(file.lastModified());
                if (fileLastModified.before(calendar.getTime())) {
                    file.delete();
                    System.out.println(file.getName());
                }
            }
            Thread.sleep(2160000);
        }

    }
}
