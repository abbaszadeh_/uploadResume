package com.bisphone.controller;

import com.bisphone.service.TusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
@RequestMapping("/files")
public class AttachmentController {

    @Autowired
    TusService service;

    @PostMapping(value = {"", "/"})
    public Mono<ResponseEntity> init(@RequestHeader(value = "Upload-Length", required = false) Long uploadSize,
                                     @RequestHeader(value = "Upload-Metadata", required = false) String uploadMeta) {
        return service.createUpload(uploadMeta);
    }


    @RequestMapping(value = {"/{id}", "/{id}}/"}, method = {RequestMethod.HEAD})
    public Mono<ResponseEntity> headProcessJs(@PathVariable("id") String id,
                                            @RequestHeader(value = "Upload-Length" , required = false) Long length,
                                            HttpServletRequest request) throws IOException {
        return service.headProcessing(length,id, request);
    }

}
