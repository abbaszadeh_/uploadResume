package com.bisphone.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class ServerController {
    @RequestMapping(value = {"", "/files"}, method = {RequestMethod.OPTIONS})
    public Mono<ResponseEntity> status() {
        System.out.println("Options happened!!!");
        return Mono.just(ResponseEntity.noContent()
                .header("Tus-Version", "1.0.0")
                .header("Tus-Extension", "creation,creation-defer-length,expiration,termination")
                .header("Tus-Max-Size", "2000000000")  // 2GB
                .build());

    }

    @GetMapping("/files")
    public void getProcessing(){
        System.out.println("get Request Mapping");
    }
}