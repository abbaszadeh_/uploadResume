package com.bisphone.controller;

import com.bisphone.service.TusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Response;
import java.io.IOException;

@RestController
@RequestMapping("/files")
public class UploadController {


    @Autowired
    private TusService service;


    @RequestMapping(value = "/{id}", method = {RequestMethod.POST,RequestMethod.PATCH})
    public Mono<ResponseEntity> patchProcessing(@RequestHeader(value = "Content-Type", required = false) String contentType,
                                                @RequestHeader(value = "Upload-Offset", required = false) Long offset,
                                                @RequestHeader(value = "Upload-Length", required = false) Long uploadLength,
                                                @PathVariable(value = "id") String id,
                                                HttpServletRequest request) throws IOException {

        System.out.println(request.getMethod()+ "  Upload");
        return service.upload(id, request, uploadLength);
    }
}
