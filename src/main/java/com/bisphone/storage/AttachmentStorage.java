package com.bisphone.storage;


import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Component;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class AttachmentStorage {

    String baseLocation = "src/main/resources/";

    public void write(InputStream inputStream , String id) throws IOException {
        String fileName =new String(DatatypeConverter.parseBase64Binary(id));

        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(baseLocation+fileName),true));
        ByteArrayOutputStream baos= new ByteArrayOutputStream();
        IOUtils.copy(inputStream,baos);
        bos.write(baos.toByteArray());
        baos.flush();

    }

    public long getUploadedSize(String id) throws IOException {

        String fileName =new String(DatatypeConverter.parseBase64Binary(id));

        if (fileExist(id)){
            return Files.size(Paths.get(baseLocation + fileName));
        }
        else return -1;
    }

    public boolean fileExist(String id){
        String fileName =new String(DatatypeConverter.parseBase64Binary(id));
        return Files.exists(Paths.get(baseLocation + fileName));
    }
}
