package com.bisphone;

import com.bisphone.tusClient.MyTusClient;
import io.tus.java.client.ProtocolException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
@RunWith(SpringRunner.class)
public class TestController {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Autowired
    WebTestClient webTestClient;


    @Value("${storage.uploadFile}")
    private String file;


    @Test
    public void test1_upload_Expected_200StatusCode() {
        try {

            String fileName = "film.mkv";

            RandomAccessFile raf = new RandomAccessFile(new File("src/test/resources/" + fileName), "rw");
            long realSize = raf.length();
            List<String> strings = webTestClient.head().uri("/files/get/uploaded/size?fileName=" + fileName)
                    .exchange().expectBody().returnResult().getResponseHeaders().get("upload-offset");

            long uploadedSize = Long.valueOf(strings.get(0));

            boolean f = false;

            int sizeBuffer = 256 * 1024;
            byte[] buffer = new byte[sizeBuffer];

            MultiValueMap<String, Object> formData;

            WebTestClient.ResponseSpec exchange = null;

            System.out.println("first uploaded Size ; " + uploadedSize);

            raf.seek(uploadedSize);
            while (raf.read(buffer) != -1) {
                formData = new LinkedMultiValueMap<>();
                formData.add("fileName", fileName);
                formData.add("chunk", buffer);
                formData.add("length", realSize);

                exchange = webTestClient.put().uri("/files/upload/resume")
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .body(BodyInserters.fromMultipartData(formData))
                        .exchange();
                exchange.expectStatus().isOk();
                if (exchange.expectBody().returnResult().getStatus().is5xxServerError()) {
                    return;
                }


                if (uploadedSize + 256 * 1024 > realSize) {
                    sizeBuffer = ((int) (realSize - uploadedSize));
                    System.out.println(sizeBuffer);
                    uploadedSize = uploadedSize + sizeBuffer;
                    System.out.println(uploadedSize);
                    buffer = new byte[sizeBuffer];
                    f = true;
                } else uploadedSize = uploadedSize + sizeBuffer;

                if (f) System.out.println(uploadedSize);
//            System.out.println(uploadedSize);

                float percent = ((float) uploadedSize / realSize * 100);
                System.out.format("%.2f\n", percent);
            }
            if (exchange != null)
                exchange.expectStatus().isOk();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("channel closed!!!");
        }
    }


    @Test
    public void tusClientTest() throws IOException, ProtocolException {
            MyTusClient testClient = new MyTusClient();
            testClient.simpleUpload();
    }

    @Test
    public void test_tusResumableUpload() throws IOException, ProtocolException {
            MyTusClient client = new MyTusClient();
            client.uploadResume();
    }

}